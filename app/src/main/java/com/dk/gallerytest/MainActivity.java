package com.dk.gallerytest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();

        final ArrayList<Integer> imgs = new ArrayList<>();
        imgs.add(R.drawable.img_1);
        imgs.add(R.drawable.img_2);
        imgs.add(R.drawable.img_3);
        imgs.add(R.drawable.img_4);
        imgs.add(R.drawable.img_5);
        imgs.add(R.drawable.img_6);
        imgs.add(R.drawable.img_7);
        imgs.add(R.drawable.img_8);
        imgs.add(R.drawable.img_9);
        imgs.add(R.drawable.img_10);
        imgs.add(R.drawable.img_11);
        imgs.add(R.drawable.img_12);

        GridView gridView = (GridView) findViewById(R.id.gridView);
        GridAdapter gridAdapter = new GridAdapter(this, R.layout.grid_item, imgs);
        gridView.setAdapter(gridAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                ImageView imageView = (ImageView) v.findViewById(R.id.ivGridImg);
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                int[] screenLocation = new int[2];
                imageView.getLocationOnScreen(screenLocation);
                intent.putExtra(DetailsActivity.KEY_THUMB_LEFT, screenLocation[0])
                        .putExtra(DetailsActivity.KEY_THUMB_TOP, screenLocation[1])
                        .putExtra(DetailsActivity.KEY_THUMB_WIDTH, imageView.getWidth())
                        .putExtra(DetailsActivity.KEY_THUMB_HEIGHT, imageView.getHeight())
                        .putExtra(DetailsActivity.KEY_IMG, imgs.get(position));
                startActivity(intent);
            }
        });
    }
}
